import {task} from "./task.js";
export class taskManager{

    constructor(){
        this.tasks = [];
        this.ids = 0;
    }

    getTaskName(id){
        let tempTask = this.tasks.find((task)=>task.id == id);
        if(tempTask){
            return tempTask.getName();
        }
    }

    getTasks(){
        return this.tasks;
    }

    addTask(name){
        this.ids += 1;
        this.tasks.push(new task(name, this.ids));
    }

    deleteTask(id){
        let tempTask = this.tasks.find((task)=>task.id == id);
        if(tempTask){
            let index = this.tasks.indexOf(tempTask);
            this.tasks.splice(index,1);
        }
    }

    completeTask(id){
        let tempTask = this.tasks.find((task)=>task.id == id);
        if(tempTask){
            let index = this.tasks.indexOf(tempTask);
            this.tasks[index].complete = true;
        }
    }

    nameRater(id, newName) {
        let tempTask = this.tasks.find((task) => task.id == id);
        if(tempTask){
            let index = this.tasks.indexOf(tempTask);
            this.tasks[index].name = newName;
        }
    }
}


