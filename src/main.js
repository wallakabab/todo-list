//import {task} from task.js;
import { taskManager } from "./taskManager.js";


let tasks = new taskManager;



const addTaskButton = document.getElementById("newTaskButton");
let buttons = document.querySelectorAll("button");
const modal = document.getElementById("renameModal");
const modalTitle = document.getElementById("modalTitle");
const modalButton = document.getElementById("modalButton");

addTaskButton.addEventListener('click', ()=>{
    if(document.getElementById("newTaskInput").value != ''){
        tasks.addTask(document.getElementById("newTaskInput").value);
        document.getElementById("newTaskInput").value = '';
        console.log(tasks)
        printList();
    }
});

modalButton.addEventListener('click', ()=>{
    if(document.getElementById("rename").value != ''){
        tasks.nameRater(modalTitle.title, document.getElementById("rename").value)
    }
    modal.style.display = "none";
    printList();
    document.getElementById("rename").value = '';
})

window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  }

function printList(){
    const cardDeckToDo = document.querySelector('.to-do-deck');
    const cardDeckDone = document.querySelector('.done-deck');
    cardDeckToDo.innerHTML = '';
    cardDeckDone.innerHTML = '';
    tasks.getTasks().forEach((task)=>{
        const card = document.createElement('div');
        const emptyCard = document.createElement('div');
        card.classList.add('card', 'fade-in');

        
        if(task.getComplete()){
            card.innerHTML = `
                <p class="card-text" id ="${task.getId()}">${task.getName()}
                <button disabled class="mark-complete" id = "${task.getId()}"></button>
                <button disabled class="delete" id = "${task.getId()}"></button>
                <button disabled class="rename" id = "${task.getId()}"></button></p>
            `;
            cardDeckDone.appendChild(card);
        }
        else{
            card.innerHTML = `
                <p class="card-text" id ="ct${task.getId()}">${task.getName()}
                <button class="mark-complete" id = "${task.getId()}"></button>
                <button class="delete" id = "${task.getId()}"></button>
                <button class="rename" id = "${task.getId()}"></button></p>
            `;
            cardDeckToDo.appendChild(card);
        }
        buttons = document.querySelectorAll("button");
        buttons.forEach(button => {
            button.addEventListener("click", (event)=>{
                if(event.target.className == 'delete'){
                    console.log(event.target.id);
                    tasks.deleteTask(event.target.id);
                    printList();
                }
                if(event.target.className == 'mark-complete'){
                    tasks.completeTask(event.target.id);
                    printList();
                }
                if(event.target.className == 'rename'){ //TODO
                    modal.style.display = "block";
                    modalTitle.innerHTML = "So, you want to rename " + tasks.getTaskName(event.target.id) + '?';
                    modalTitle.title = event.target.id;
                }
            });
        });
    })
}


