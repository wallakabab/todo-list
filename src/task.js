export class task{
    constructor(name, id){
        this.name = name;
        this.id = id
        this.complete = false;
    }

    getName(){
        return this.name;
    }
    getId(){
        return this.id;
    }
    getComplete(){
        return this.complete;
    }
    setComplete(){
        this.complete = true;
    }
}
